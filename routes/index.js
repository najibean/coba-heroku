const { Router } = require('express');
const router = Router();
const userRoutes = require('./user')
const movieRoutes = require('./movie')

router.get('/', (req,res)=>{
    res.status(200).json({
        message : "This is home page lhooooo."
    })
});
router.use('/users', userRoutes)
router.use('/movies', movieRoutes)

module.exports = router;