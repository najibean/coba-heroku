const { Router } = require('express');
const router = Router();
const UserController = require('../controllers/user')
const imageMulter = require('../middlewares/multer')

router.get('/', UserController.list)
router.post('/login', UserController.login)
router.post('/register', imageMulter, UserController.register)
router.get('/profile/:id', UserController.profile)
router.put('/editprofile/:id', imageMulter, UserController.editUser)
router.delete('/delete/:id', UserController.deleteUser)


// router.get('/login', (req, res)=>{
//     res.status(200).json({
//         message: 'Login'
//     })
// })

// router.get('/register', (req, res)=>{
//     res.status(200).json({
//         message: 'Register'
//     })
// })

module.exports = router;