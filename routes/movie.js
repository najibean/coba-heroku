const { Router } = require('express');
const router = Router();
const MovieController = require('../controllers/movie')

// const { authentication, authorization } = require('../middlewares/authAdmin')

router.get('/',MovieController.getMovie)
router.get('/details/:id', MovieController.movieDetails)
router.post('/add', MovieController.addMovie)
router.delete('/delete/:id', MovieController.deleteMovie)
router.put('/edit/:id',MovieController.editMovie)

module.exports = router;